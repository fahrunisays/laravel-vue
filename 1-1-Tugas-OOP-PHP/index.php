<?php

trait hewan{
    public $nama;
    public $jumlahkaki;
    public $keahlian;
    public $darah = 50; 
}

abstract class fight{
    use hewan; //dimasukkan disini jadi pas diintansiasi bisa langsung 2
    public $attackpower;
    public $defencepower;

    public function atraksi(){
        echo "{$this->nama} si jago {$this->keahlian}";
        echo "<br>";
    }

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";

        $hewan->diserang($this);
        
    }

    public function diserang($hewan){
        echo "{$this->nama}sedang diserang {$hewan->nama}";
        echo "<br>";
        //darah = darah sekarang - (attackPower ((penyerang)) / defencePower yang ((diserang)))
        $this->darah = $this->darah - ($hewan->attackpower/$this->defencepower);
    }  

    protected function getInfo(){
        echo "<br>Nama:  {$this->nama} <br>";
        echo "Jumlah Kaki:  {$this->jumlahkaki} <br>";
        echo "Keahlian:  {$this->keahlian} <br>";
        echo "Sisa Darah:  {$this->darah} <br>";
        echo "Attack Power:  {$this->attackpower} <br>";
        echo "Defence Power:  {$this->defencepower} <br>";
        $this->atraksi();

    }

    //fungsi abstrak buat manggil getinfo hewan
    abstract public function getInfoHewan();

}

//Intansiasi child
class elang extends fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackpower = 10;
        $this->defencepower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Elang";
        $this->getInfo();
    }
}
class harimau extends fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->defencepower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Harimau";
        $this->getInfo();
    }
}

//kasih baris biar rapi
class baris{
    public static function tampilbaris(){
        echo "<br>";
        echo "----------------------------";
        echo "<br>";
    }
}

$elang = new elang("elang 1.0");
$harimau = new harimau("harimau 2.0");

$elang->getInfoHewan();

baris::tampilbaris();

$harimau->getInfoHewan();

$harimau->serang($elang);
baris::tampilbaris();
$elang->getInfoHewan();
